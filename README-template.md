> A template for a README for an open source program. Make a copy and
> edit as needed for your program.

> Quoted text like this is commentary and should be deleted when using
> the template.

> If the README grows too long, it's a good idea to move longer
> sections into separate files and link to them from the README. An
> effective README is often fairly short. Keep the summary, example,
> and legal stuff.

# README-template.md - a template for a README

This is [INSERT 25 WORD SUMMARY HERE].

> The summary should aim to be comprehensible to anyone stumbling up
> on the program, but should not have many details. The summary is
> meant to quickly answer the question "who would use this and for
> what purpose?". The primary audience of the summary is people who
> find it by accident and need to know what the program does, so they
> can quickly decide if they should spend effort on finding out more.
>
> The summary is arbitrarily limited to 25 words to motivate you to
> make it short. There is no punishment if you exceed the limit.
>
> Add a screenshot here if possible. Even if it's of a terminal,
> though a text-only "screenshot" may be enough in that case.

## Example

A [USER PERSONA] would use this program to [GOAL] like this:

```sh
$ COMMAND OPTION ARG
OUTPUT
$ 
```

> The example should come early in the README. People like to learn
> from examples.
>
> Examples should ideally show inputs and outputs as well. The first
> example should probably be of the simplest, most basic way of using
> the program, and more advanced or esoteric use cases can be shown in
> other examples or in a separate user guide or manual.

## Install

> A short explanation of where to download and how to install the
> software locally, or a pointer to a longer description of this.

## Using

> A short description of how to use the software, once it's installed.
> This is primarily applicable to locally installed software. A full
> manual should probably go to another document, in which case there
> should be a pointer to that document here.

## Discussion

> A longer discussion about the program, what it does, and whatever
> else is useful. This can point to a user guide or other manual.

## Stakeholders

> List named people or groups, or descriptions of types of users,
> whose needs and wants need to be considered when deciding on what
> requirements. Don't forget "the maintainer" as a stakeholder.

## Requirements

> List each requirement here as a bullet point. If a requirement is
> too big on its own, break it down into sub-requirements, as nested
> bullet points. It's probably good to use RFC 2119 terminology (MUST,
> MUST NOT...).
>
> For extra credit, mention which stakeholders especially care about
> each requirement, and how to verify the requirement is met.

- MUST work.
  - MUST NOT delete user data.
  - SHOULD NOT publish the password one the New York Times front page.

## Architecture

> The architecture description should have at least two levels.
>
> - Level 0: the software and its role in the universe. Which systems
>   and people does the software interact with and how?
>
> - Level 1: major internal components and how they interact. This can
>   include databases, message buses, and other major components.
>   Where the draw the line between levels 0 and 1 needs careful
>   thought on a case-by-case basis.
>
> Add more levels as needed. Consider separate views on static
> components, deployment, run time communication, and anything else
> needed to understand the system.
>
> Use of useful diagrams in the description of the architecture has
> been approved. MermaidJS or other diagram markup languages may be
> useful.

## Building

> Instructions for building the software, preferably as shell commands
> that can be copy-pasted. List dependencies here, as well.

## Testing

> Instructions for running the automated test suite. Again, preferably
> as shell commands to copy-paste.

## Deployment

> List any special considerations for deploying the software, whether
> to production or development or QA or wherever. How to build a
> package, if any, can be described here, even if it is just saying
> "make deb". This is for deployment to a server, where "Installing"
> is more for local installs. The two sections can also be merged, if
> that's clearer.

## Contributing

> Explain how to contribute to the program, whether code,
> documentation, or something else.

## Legalese

Copyright 2023 Lars Wirzenius.

This software (the README template) is licensed under the Creative
Commons CC0 1.0 Universal license. See
<https://creativecommons.org/publicdomain/zero/1.0/> for details.
Basically, use as you wish.

> You need to replace this section entirely, if you make a README
> based on the template.
